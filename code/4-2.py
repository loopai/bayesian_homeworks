#%%
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

np.random.seed(42)

#%%
n_samples = 5000
theta_A = np.random.gamma(237, 1/20, n_samples)
theta_B = np.random.gamma(125, 1/14, n_samples)
plt.figure(figsize=(12, 8))
plt.hist(theta_A, alpha=0.5)
plt.hist(theta_B, alpha=0.5)
plt.legend(['theta_A', 'theta_B'])
plt.show()

# %%
prob_diff = np.mean(theta_B < theta_A)
print('Probability of theta_B < theta_A: ', prob_diff)

all_n0 = [int(x) for x in np.arange(0, 1001, 25)]
all_prob_diff = []
for n0 in all_n0:
    temp_theta_B = np.random.gamma(12 * n0 + 113, 1 / (n0 + 13), n_samples)
    temp_prob_diff = np.mean(temp_theta_B < theta_A)
    all_prob_diff.append(temp_prob_diff)
    # print('n0: ', n0, '- prob diff: ', temp_prob_diff)
    
plt.figure(figsize=(12, 8))
plt.scatter(all_n0, all_prob_diff)
plt.show()
