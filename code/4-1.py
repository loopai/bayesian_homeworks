#%%
import matplotlib.pyplot as plt
import numpy as np

np.random.seed(42)

#%%
n_samples = 5000
theta_1 = np.random.beta(58, 44, n_samples)
theta_2 = np.random.beta(31, 21, n_samples)
plt.figure(figsize=(12, 8))
plt.xlim((0, 1))
plt.hist(theta_1, alpha=0.5)
plt.hist(theta_2, alpha=0.5)
plt.legend(['theta_1', 'theta_2'])
plt.show()

# %%
prob_diff = np.mean(theta_1 < theta_2)
print('Probability of theta_1 < theta_2: ', prob_diff)
