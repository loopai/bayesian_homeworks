#%% importing libraries
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, chain

plt.style.use('default')
np.random.seed(42)
n_samples = 1000

#%% defining some variables
all_k0 = [int(2**x) for x in range(18)]
print('All k0 value: ', all_k0)
mu0 = 75
sigma0 = 10
n_A = 16
n_B = 16
mu_A = 75.2
mu_B = 77.5
sigma_A = 7.3
sigma_B = 8.1

#%% defining sigman and mun functions of k0, nu0
def sigman_A(k0, nu0):
    return (
        1/(nu0 + n_A) * (
            nu0 * sigma0**2 +
            (n_A - 1) * sigma_A**2 +
            k0*n_A/(k0 + n_A) * (mu_A - mu0)**2
        )
    )
def sigman_B(k0, nu0):
    return (
        1/(nu0 + n_B) * (
            nu0 * sigma0**2 +
            (n_B - 1) * sigma_B**2 +
            k0*n_B/(k0 + n_B) * (mu_B - mu0)**2
        )
    )
def mun_A(k0): return (mu0 * k0 + n_A * mu_A) / (k0 + n_A)
def mun_B(k0): return (mu0 * k0 + n_B * mu_B) / (k0 + n_B)

#%% Monte Carlo sampling
all_post_sigma_A = []
all_post_sigma_B = []
all_post_mu_A = []
all_post_mu_B = []

for i in range(len(all_k0)):
    all_post_sigma_A.append(
        1/np.random.gamma(
            (all_k0[i] + n_A) / 2,
            1 / (sigman_A(all_k0[i], all_k0[i]) * (all_k0[i] + n_A) / 2 ),
            n_samples
        )
    )
    all_post_sigma_B.append(
        1/np.random.gamma(
            (all_k0[i] + n_B) / 2,
            1 / (sigman_B(all_k0[i], all_k0[i]) * (all_k0[i] + n_B) / 2 ),
            n_samples
        )
    )
    all_post_mu_A.append(
        [
            np.random.normal(
            mun_A(all_k0[i]), np.sqrt( x / (all_k0[i] + n_A) )
            )  for x in all_post_sigma_A[i]
        ]
    )
    all_post_mu_B.append(
        [
            np.random.normal(
            mun_B(all_k0[i]), np.sqrt( x / (all_k0[i] + n_B) )
            )  for x in all_post_sigma_B[i]
        ]
    )

#%% plotting distribution of mu_A in function of k0
for post in all_post_mu_A:
    plt.hist(post)

#%% plotting distribution of mu_B in function of k0
for post in all_post_mu_B:
    plt.hist(post)

#%% computing the probability of theta_A < theta_B in function of k0
all_diff_list = [
    theta_A < theta_B for theta_A, theta_B in zip(
        chain.from_iterable(all_post_mu_A),
        chain.from_iterable(all_post_mu_B)
    )
]
all_diff = [
        all_diff_list[x: x+n_samples]
        for x in range(0, len(all_diff_list), n_samples)
]

#%% plotting the probability of theta_A < theta_B in function of log(k0)
all_prob_diff = [np.sum(diff)/len(diff) for diff in all_diff]
plt.figure(figsize=(12, 8))
plt.title(r'Probability of $\theta_A < \theta_B$ in function of $\log(k_0)$',
        size=14)
plt.xticks(np.log(all_k0), labels=[str(x) for x in all_k0])
plt.xlabel(r'$\log(k_0)$', size=14)
plt.ylabel(r'$\mathbb{P}(\theta_A < \theta_B)$', size=14)
plt.grid(True, alpha=0.4)
for i in range(len(all_k0) - 1):
    plt.plot(
            np.log(all_k0[i: i+2]), all_prob_diff[i: i+2],
            'o-', color='green', markersize=8
    )
#plt.savefig('../figures/5-2_prob_diff_log_scale.png')

