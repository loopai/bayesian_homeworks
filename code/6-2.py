#%%
import requests
from string import punctuation, whitespace
import matplotlib.pyplot as plt
import seaborn as sns

#%%
glucose_url = (
    'http://wwwlegacy.stat.washington.edu/'
    'people/pdhoff/Book/Data/hwdata/glucose.dat'
)

raw_data = requests.get(glucose_url).text
data = [int(x) for x in raw_data if x not in punctuation + whitespace]
fig, ax = plt.subplots(figsize=(12, 8))
# sns.set_style('whitegrid')
sns.distplot(data, ax=ax)
plt.show()
# %%
