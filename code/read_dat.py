def read_dat(filename):
    symbols = ["\n", " "]
    data = []
    counter = 0
    with open(filename) as f:
        print(counter)
        data += f.read()
        counter += 1
    return [int(x) for x in data if x not in symbols]


if __name__ == "__main__":
    filename = "./data/menchild30bach.dat"
    data = read_dat(filename)
    print(data)
    print(len(data))
    read_data = [
        1,
        0,
        0,
        1,
        2,
        2,
        1,
        5,
        2,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        1,
        1,
        0,
        0,
        0,
        1,
        1,
        2,
        1,
        3,
        2,
        0,
        0,
        3,
        0,
        0,
        0,
        2,
        1,
        0,
        2,
        1,
        0,
        0,
        1,
        3,
        0,
        1,
        1,
        0,
        2,
        0,
        0,
        2,
        2,
        1,
        3,
        0,
        0,
        0,
        1,
        1,
    ]
    print("len real data: ", len(read_data))
