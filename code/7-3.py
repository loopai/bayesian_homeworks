#%% importing libraries and fixing some useful constants
import numpy as np
from string import punctuation, whitespace
import pandas as pd
from IPython.display import display
from scipy.stats import invwishart
import matplotlib.pyplot as plt

def Stheta(y, theta):
    'y: matrix, theta: vector of relative fixed means -> Stheta'
    if (isinstance(y, pd.DataFrame)):
        y = y.values
    diff = y - theta
    return np.sum(np.matmul(diff, np.transpose(diff)))


RANDOM_STATE = 42

bluecrab_fn = "./data/bluecrab.dat"
orangecrab_fn = "./data/orangecrab.dat"

#%% reading datasets and reporting some noteworthy informations
bluecrab = pd.read_csv(bluecrab_fn, names=['depth', 'width'], sep=' ')
orangecrab = pd.read_csv(orangecrab_fn, names=['depth', 'width'], sep=' ')

n_examples, n_vars = bluecrab.shape
print(f'n_examples = {n_examples}, n_vars = {n_vars}')
print('head and tail of the bluecrab:')
display(pd.concat((bluecrab.head(), bluecrab.tail())))
#%% gibbs and trace plots
def gibbs(data, n_iterations = 10000):
    'data -> (theta, sigma)'
    mu = data.mean(axis=0)
    mu0 = mu
    nu0 = 4
    A0 = np.cov(data, rowvar=False)
    S0 = A0 
    theta = [0] * n_iterations
    sigma = [0] * ( n_iterations + 1)
    sigma[0] = S0

    for i in range(n_iterations):
        S = sigma[i]
        A = np.linalg.inv(A0) + n_examples * np.linalg.inv(S)
        b = np.matmul(
            np.linalg.inv(A0), mu0) + n_examples * np.matmul(np.linalg.inv(S),
            mu
        )
        theta[i] = np.random.multivariate_normal(
            np.matmul(b, np.linalg.inv(A)),
            np.linalg.inv(A)
        )
        sigma[i + 1] = invwishart.rvs(
            df=( nu0 + n_examples ),
            scale=( S0 + Stheta(data, theta[i]) ),
            random_state=RANDOM_STATE
        )
    return (theta, sigma[:n_iterations])


def trace_plot(theta, sigma, average_red_line=True,
            n_iterations=10000, crab_color='blue'):
    plt.style.use('default')
    fig, ax = plt.subplots(4, 1, figsize=(10, 10))
    ax[0].plot(np.arange(n_iterations), [x[0] for x in theta])
    ax[1].plot(np.arange(n_iterations), [x[1] for x in theta])
    ax[2].plot(np.arange(n_iterations), [x[0][0] for x in sigma])
    ax[3].plot(np.arange(n_iterations), [x[1][1] for x in sigma])
    if average_red_line:
        ax[0].axhline(y=np.mean([x[0] for x in theta]), color='red')
        ax[1].axhline(y=np.mean([x[1] for x in theta]), color='red')
        ax[2].axhline(y=np.mean([x[0] for x in sigma]), color='red')
        ax[3].axhline(y=np.mean([x[1] for x in sigma]), color='red')
    ax[0].set_ylabel(r'$\theta_{depth}$', fontsize=14)
    ax[1].set_ylabel(r'$\theta_{width}$', fontsize=14)
    ax[2].set_ylabel(r'$\Sigma_{depth}$', fontsize=14)
    ax[3].set_ylabel(r'$\Sigma_{width}$', fontsize=14)
    fig.suptitle(f'{crab_color} crabs trace plots', fontsize=16)
    # plt.savefig(f'../figures/7-3_{crab_color}_trace_plots.png')
    plt.show()


#%% (a)
theta_blue, sigma_blue = gibbs(bluecrab)
theta_orange, sigma_orange = gibbs(orangecrab)
#%%
#%% bivariate trace plots
fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(12, 12))
ax1.scatter(
    [x[0] for x in theta_blue], [x[1] for x in theta_blue],
    color='blue', marker='.'
)
ax1.set_title('Bivariate trace plots for blue crabs')
ax1.set_xlabel('depth')
ax1.set_ylabel('width')
ax2.scatter(
    [x[0] for x in theta_orange], [x[1] for x in theta_orange],
    color='orange', marker='.'
)
ax2.set_title('Bivariate trace plots for orange crabs')
ax2.set_xlabel('depth')
ax2.set_ylabel('width')
# plt.savefig('../figures/7-3_bivariate_trace_plots.png')
plt.show()
#%%
trace_plot(theta_blue, sigma_blue, crab_color='blue')
#%%
trace_plot(theta_orange, sigma_orange, crab_color='orange')
#%%
corr_blue = [
    x[0][1]/(np.sqrt(x[0][0]) * np.sqrt(x[1][1])) for x in sigma_blue
]
corr_orange = [
    x[0][1]/(np.sqrt(x[0][0]) * np.sqrt(x[1][1])) for x in sigma_orange
]
#%%
plt.figure(figsize=(12, 6))
plt.hist(corr_blue, alpha=0.5)
plt.hist(corr_orange, alpha=0.5)
plt.legend(['blue', 'orange'])
plt.title('Distributions of the correlations', fontsize=16)
# plt.savefig('../figures/7-3_corr_distributions.png')
plt.show()
#%%
print(
    'Prob (rho_blue < rho_orange): ',
    np.mean([x < y for x, y in zip(corr_blue, corr_orange)])
)