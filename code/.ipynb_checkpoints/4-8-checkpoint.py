#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import gamma


def read_dat(filename):
    symbols = ['\n', ' ']
    data = []
    with open(filename) as f:
        data += f.readline()
    return [int(x) for x in data if x not in symbols]

#%%
bach_filename = "./data/menchild30bach.dat"
nobach_filename = "./data/menchild30nobach.dat"
bach = np.sort(read_dat(bach_filename))
nobach = np.sort(read_dat(nobach_filename))

print("bach data:\n", bach)
print("no bach data:\n", nobach)
#%%
n = 5000
supp = np.linspace(0, 120, n)
prior = gamma.pdf(supp, 2, 1)
post_bach = gamma.pdf(supp, np.sum(bach) + 2, len(bach) + 1)
post_nobach = gamma.pdf(supp, np.sum(nobach) + 2, len(nobach) + 1)
plt.figure(figsize=(12, 8))
plt.plot(supp, prior)
plt.plot(supp, post_bach)
plt.plot(supp, post_nobach)
plt.legend(["prior", "post - bach", "post - nobach"])

#%%
random_supp = np.arange(n)
np.random.shuffle(random_supp)
random_samples_A = post_bach[[int(x) for x in supp[random_supp]]]
np.random.shuffle(random_supp)
random_samples_B = post_nobach[[int(x) for x in supp[random_supp]]]
#%%
